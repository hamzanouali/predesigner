var app = new Vue({
  el: '#app',
  data: {
    // used in order to detect wich DOM element is selected
    oneSelectedDomTarget: null,

    HTML5Tags: ['div','p','span','a','br','hr','h1-h6','','article','section','aside','nav','header','footer','details','summary'],

    // list of elements that does not accept inner text
    notTextAble: ['div','form','ul','br','hr'],

    // list of allowed elements to be resized
    resizableElements: ['div','button'],

    // elements that can be resized without restriction
    resiableWithZeroRestriction : ['button'],

    // current target to be resized or moved
    target: null, 

    components: {
      bulma: {
        navbar: `<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="https://bulma.io">
      <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item">
        Home
      </a>

      <a class="navbar-item">
        Documentation
      </a>

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          More
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            About
          </a>
          <a class="navbar-item">
            Jobs
          </a>
          <a class="navbar-item">
            Contact
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Report an issue
          </a>
        </div>
      </div>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a class="button is-light">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>`,

        menu : `<aside class="menu">
  <p class="menu-label">
    General
  </p>
  <ul class="menu-list">
    <li><a>Dashboard</a></li>
    <li><a>Customers</a></li>
  </ul>
  <p class="menu-label">
    Administration
  </p>
  <ul class="menu-list">
    <li><a>Team Settings</a></li>
    <li>
      <a class="is-active">Manage Your Team</a>
      <ul>
        <li><a>Members</a></li>
        <li><a>Plugins</a></li>
        <li><a>Add a member</a></li>
      </ul>
    </li>
    <li><a>Invitations</a></li>
    <li><a>Cloud Storage Environment Settings</a></li>
    <li><a>Authentication</a></li>
  </ul>
  <p class="menu-label">
    Transactions
  </p>
  <ul class="menu-list">
    <li><a>Payments</a></li>
    <li><a>Transfers</a></li>
    <li><a>Balance</a></li>
  </ul>
</aside>`
      }
    }

  },

  mounted() {

    this.watchSelectedDOMElements();

    this.initContextMenu();

    $("#demo01").animatedModal();

    this.dbClick()

  },

  methods: {

    /**
     * listen to dbclick event
     */
    dbClick(){

      document.addEventListener('dblclick', e => {

        let newClass = prompt('Update Class', e.target.className)

        if(newClass) e.target.className = newClass
      })

    },

    /**
     * creates DOM element
     * @param {string} elem contains the tagName in lower case
     */
    create(elem) {

      // to lower case
      elem = elem.toLocaleLowerCase()

      let element = document.createElement(elem)

      // prompt class
      let classname =  prompt('insert class', '')

      // insert text for non-div elements
      let demoText = ''
      if(! this.notTextAble.includes(elem)) {
        demoText = prompt('insert some text', 'demo text')
      }

      // asign class to element
      element.className = classname 
      element.textContent = demoText

      // put the created element inside the selected parent
      let target = this.oneSelectedDomTarget != null ? this.oneSelectedDomTarget : document.getElementById('section')

      // append to body
      target.appendChild(element)

    },

    /**
     * inserts Bulma built-in components
     * @param {string} componentName
     */
    insertComponent(componentName){

      // to lower case
      componentName = componentName.toLowerCase()

      // get HTML
      /**
       * @param {String} HTML representing a single element
       * @return {Element}
       */
      let htmlToElement = (html) => {
          var template = document.createElement('template');
          html = html.trim(); // Never return a text node of whitespace as the result
          template.innerHTML = html;
          return template.content.firstChild;
      }

      let element = htmlToElement(this.components.bulma[componentName])

      // push html 
      // put the created element inside the selected parent
      let target = this.oneSelectedDomTarget != null ? this.oneSelectedDomTarget : document.getElementById('section')

      // append to body
      target.appendChild(element)

    },

    /**
     * watch any for any new DOM elements
     */
    watchSelectedDOMElements(){
      // Options for the observer (which mutations to observe)
      let config = { childList: true, subtree: true };

      // Callback function to execute when mutations are observed
      let callback = (mutationsList, observer) => {
        for(let mutation of mutationsList) {
            if (mutation.type == 'childList') {
                
              // add event listener 
              this.addEventListenerToDocument()

            }
        }
      };

      // Create an observer instance linked to the callback function
      var observer = new MutationObserver(callback);

      // Start observing the target node for configured mutations
      observer.observe(document.body, config);
    },

    /**
     * adds event listener to document 
     */
    addEventListenerToDocument() {

      document.getElementById('section').addEventListener('click', (event) => {
        
        this.target = event.target

        if(this.target != this.oneSelectedDomTarget){

          // blur the previous selected target
          if(this.oneSelectedDomTarget != null) {
            this.oneSelectedDomTarget.style.outline = 'none'
            this.oneSelectedDomTarget.style.resize = 'none'
          }

          // focus on the new target
          this.oneSelectedDomTarget = this.target
          this.target.style.outline = `1px solid #000`
          this.target.style.resize = 'vertical'
          this.target.style.overflow = 'auto'

          if(this.resizableElements.includes(event.target.tagName.toLowerCase())) {
            // moving element using margins
            window.addEventListener('keydown', e => {
              e.preventDefault()
              this.changeMargins(e.key, this.target)
            })
          }
        } 

      })

    },

    /**
     * change margins of given DOM element
     */
    changeMargins(key, target){

      // will contains the current attacked margin e.g: marginTop
      let margin = ''
      // will contain the action (increase, decrease)
      let action = ''

      switch (key) {
        case 'ArrowUp':
          margin = 'marginTop'
          action = 'decrease'
          break;
        case 'ArrowDown':
          margin = 'marginTop'
          action = 'increase'
          break;
        case 'ArrowLeft':
          margin = 'marginRight'
          break;
        case 'ArrowRight':
          margin = 'marginLeft'
          break;
      }

      // get margin value
      let marginValue = this.target.style[margin];

      // convert marginValue to Number
      // 1- remove css units from string
      marginValue = marginValue.replace('px','')
      marginValue = Number(marginValue)

      // increase/ decrease marginValue value
      if(action == 'increase')
        marginValue += 1
      else 
        marginValue -= 1

      this.target.style[margin] = marginValue + 'px'

    },

    /**
     * makes DOM elements dragable and resisable
     * @param {object} target 
     */
    useInteractJs(target){

      // contains the target name in lower case
      let targetName = event.target.tagName.toLowerCase()

      interact(event.target)
      .draggable({
        onmove: window.dragMoveListener,
        restrict: {
          restriction: 'parent',
          elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
        },
      })
      .resizable({
        // resize from all edges and corners
        edges: { left: this.resiableWithZeroRestriction.includes(targetName), right: this.resiableWithZeroRestriction.includes(targetName), bottom: true, top: true },

        // keep the edges inside the parent
        restrictEdges: {
          outer: 'parent',
          endOnly: true,
        },

        // minimum size
        restrictSize: {
          min: { width: 100, height: 5 },
        },

        inertia: true,
      })
      .on('resizemove', (event) => {
        var target = event.target,
            x = (parseFloat(target.getAttribute('data-x')) || 0),
            y = (parseFloat(target.getAttribute('data-y')) || 0);

        // update the element's style
        if(this.resiableWithZeroRestriction.includes(targetName)) target.style.width  = event.rect.width + 'px';
        target.style.height = event.rect.height + 'px';

        // translate when resizing from top or left edges
        x += event.deltaRect.left;
        y += event.deltaRect.top;

        target.style.webkitTransform = target.style.transform =
            'translate(' + x + 'px,' + y + 'px)';

        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);
        //target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height);
      });
    },

    /**
     * Creates custom right click menu (ContextMenu)
     */
    initContextMenu(){
   
      context.init({
          fadeSpeed: 100,
          filter: function ($obj){},
          above: 'auto',
          preventDoubleContext: true,
          compress: false
      });

      /**
       * Creates an array contains MenuObjects of h1 to h6
       * @return {array} contains MenuObject of h1 to h6
       */
      let createHeaderFrom1To6ForContextMenu = () => {
        let hList = []
        
        for (let i = 1; i <= 6; i++) {
          hList.push({
            text: `h${i}`,
            action: (e) => {
              e.preventDefault();
              this.create(`h${i}`)
            }

          })
        }
        return hList
      }

      // will contains contextMenu's MenuObjects
      let AddSubMenu = []

      // push all HTML5Tags to contextMenu
      this.HTML5Tags.forEach(element => {
        if(element == 'h1-h6') {
          AddSubMenu.push({
            text: element,
            subMenu: createHeaderFrom1To6ForContextMenu()
          })
        }else {
          
          AddSubMenu.push({
            text: element,
            action: (e) => {
              e.preventDefault();
              this.create(element)
            }

          })
        }
      });

      // attach contextMenu to Body
      context.attach('body', [
          // for adding tags
          {
              text: 'Add',
              subMenu: AddSubMenu
          },

          // for adding bulma components
          {
            text: 'components',
            subMenu: (()=>{
              // contains existed components
              let allComponents = Object.keys(this.components.bulma)
              // will contain MenuObjects
              let subMenu = []

              allComponents.forEach((component) => {
                
                subMenu.push({
                  text: component,
                  action: (e) => {
                    e.preventDefault()
                    this.insertComponent(component)
                  }
                })

              })

              return subMenu

            })()
          },

          {
            text: 'remove',
            action: e => {
              e.preventDefault()
              
              this.target.remove()

            }
          },

          {
            text: 'div details',
            action: e => {
              e.preventDefault()

              // remove the previous details-tooltips
              let allTooltips = document.querySelectorAll('.details-tooltip')

              for (let i = 0; i < allTooltips.length; i++) {
               
                allTooltips[i].remove()
                
              }

              // we want to select all divs under the target
              let allDivs = this.target.querySelectorAll('div')

              // insert span contains details to those divs
              for (let i = 0; i < allDivs.length; i++) {
                let span = document.createElement('span')
                span.setAttribute('class', 'details-tooltip bg-blue-light p-2')
                span.textContent = allDivs[i].className || allDivs[i].id
                allDivs[i].appendChild(span)
              }

            }
          }
      ]);

    }

  }
})

