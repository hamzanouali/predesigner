/**
 * extracts inline styles from html
 * @param {object} DOMElement is a DOM element
 * @return {Array} Array contains css styles
 */
export default (DOMElement) => {

  // will contains the extracted css
  let css = [
    //example
    // [ '#div.container' , 'background:#f50; height:auto; ']
  ]

  // contains DOM elements that contains style attribute
  let allElements = DOMElement.querySelectorAll('*[style]')

  // loop throughout elements
  allElements.forEach(element => {
    
    // in case element contains an empty style attribute
    if(! element.getAttribute('style').length) return false

    // other case

    let currentCss = []
    currentCss.push(element.className)
    // setup className to be a valide css query e.g: 'container mb-4' become '.container.mb-4'
    currentCss[0] = "."+currentCss[0].replace(/\s/gm, '.')

    currentCss.push(element.getAttribute('style'))

    // push into css 
    css.push(currentCss)

  });

  // return css
  return css

}