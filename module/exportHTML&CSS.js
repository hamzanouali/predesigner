/**
 * exports the created html page
 * @param {object} document is window.document
 */
export default (element) => {

  // remove inline styles
  element.removeAttribute('style')
  let elementsInlineS = element.querySelectorAll('*[style]')
  elementsInlineS.forEach(elem => {
    elem.removeAttribute('style')
  });

  // get HTML
  let html = element.innerHTML

  let pretty = require('pretty');

  /**
   * @todo check if i really need to extract css
   */
  //let cssExtractor = require('./cssExtractor');
  // console.log(cssExtractor.default(document.getElementById('section')))

  return pretty(html)

}
