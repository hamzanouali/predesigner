const pkg = require('./package')
import webpack from 'webpack'




module.exports = {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },

  /*
   ** Global CSS
   */
  css: [
    '~/assets/css/tailwind.css',
    '~/assets/scss/style.scss',
    '~/assets/scss/componentsStyle.scss',
    '~/node_modules/@fortawesome/fontawesome-free/css/all.min.css'
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      'nuxt-buefy',
      {
        materialDesignIcons: false
      }
    ],
  ],

  /*
   ** Build configuration
   */
  build: {

    plugins: [
      new webpack.ProvidePlugin({
        '_': 'lodash'
      })
    ],
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value 
        /* 'postcss-url': false,
        'postcss-nested': {},
        'postcss-responsive-type': {},
        'postcss-hexrgba': {} */
        'autoprefixer': true,

      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {
          // grid: true
        }
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {

    }
  }
}
