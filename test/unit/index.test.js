import {
  shallowMount
} from '@vue/test-utils'
import index from '../../pages/index'

describe('index', () => {
  test('mounts properly', () => {
    const wrapper = shallowMount(index)
    expect(wrapper.html()).toContain('Hello world')
  })
})
